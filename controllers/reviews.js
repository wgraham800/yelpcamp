const Review = require('../models/review')
const Campground = require('../models/campground')

module.exports.create = async (req, res) => {
    console.log(req.params)
    const review = new Review(req.body.review)
    const camp = await Campground.findById(req.params.id)
    review.author = req.user._id
    camp.reviews.push(review)
    await review.save()
    await camp.save()
    req.flash('success', 'Successfully made a review!')
    res.redirect(`/campgrounds/${camp._id}`)
} 

module.exports.delete = async (req, res) => {
    const { id, reviewId } = req.params
    await Campground.findByIdAndUpdate(id, { $pull: { reviews: reviewId } })
    const review = await Review.findByIdAndDelete(reviewId)
    console.log(
        `Deleted ${review} and removed from the associated campground`
    )
    req.flash('success', 'Successfully deleted a review!')
    res.redirect(`/campgrounds/${id}`)
}