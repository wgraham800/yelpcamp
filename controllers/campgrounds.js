const Campground = require('../models/campground')
const mbxGeocoding = require("@mapbox/mapbox-sdk/services/geocoding");
const mbxToken = process.env.MAPBOX_TOKEN
const geocoder = mbxGeocoding({accessToken: mbxToken})
const { cloudinary } = require('../cloudinary')

module.exports.index = async (req, res) => {
    const camps = await Campground.find({})
    if (camps != undefined) {
        res.render('campgrounds/index', { camps })
    }
    console.log('no camps found')
}

module.exports.renderNewForm = (req, res) => {
    res.render('campgrounds/new')
}

module.exports.show = async (req, res) => {
    const camp = await Campground.findById(req.params.id).populate({
        path:'reviews',
        populate: {
            path: 'author'
        }
    }).populate('author')
    console.log(camp)
    res.cookie('ID', req.params.id)
    res.render('campgrounds/details', { camp })
}

module.exports.create = async (req, res) => {
    const geoData = await geocoder.forwardGeocode({
        query: req.body.campground.location,
        limit: 1
    }).send()
    const campground = new Campground(req.body.campground)
    campground.geometry = geoData.body.features[0].geometry
    campground.images = req.files.map(f => ({url: f.path, filename: f.filename}))
    campground.author = req.user._id
    await campground.save()
    console.log(campground)
    req.flash('success', 'Successfully made a new campground!')
    res.redirect(`/campgrounds/${campground._id}`)
}

module.exports.renderEditForm = async (req, res) => {
    const camp = await Campground.findById(req.params.id)
    if (!camp) {
        req.flash('error', 'The requested campground could not be found')
        return res.redirect('/campgrounds')
    }
    res.render('campgrounds/edit', { camp })
}

module.exports.update = async (req, res) => {
    const { id } = req.params
    console.log(req.body)
    const camp = await Campground.findByIdAndUpdate(id, {
        ...req.body.campground,
    })
    const imgs = req.files.map(f => ({url: f.path, filename: f.filename}))
    camp.images.push(...imgs)
    await camp.save()
    if(req.body.deleteImages){
        for(let filename of req.body.deleteImages){
            await cloudinary.uploader.destroy(filename)
        }
        await camp.updateOne({$pull: {images: {filename: {$in: req.body.deleteImages}}}})
    }
    res.redirect(`/campgrounds/${camp.id}`)
}

module.exports.delete = async (req, res) => {
    const { id } = req.params
    const camp = await Campground.findById(id)
    const deletedCamp = await Campground.findByIdAndDelete(camp._id)
    console.log(`Deleted ${deletedCamp}`)
    req.flash('success', 'Successfully deleted campground!')
    res.redirect('/')
}