const mongoose = require('mongoose')
const Campground = require('../models/campground')
const { places, descriptors } = require('./seedHelpers.js')
const cities = require('./cities.js')

mongoose
    .connect('mongodb://localhost:27017/yelpcamp')
    .then(() => {
        console.log('DB connection successful!')
    })
    .catch((err) => {
        console.log(err)
        console.log('Error connecting to database')
    })

const seedNum = 300
const sample = (array) => array[Math.floor(Math.random() * array.length)]
const images = [
    {
        url: 'https://images.unsplash.com/photo-1478827536114-da961b7f86d2?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2340&q=80',
        filename: 'YelpCamp/tent2'
    },
]

const seedDB = async () => {
    await Campground.deleteMany({})
    for (let i = 0; i < seedNum; i++) {
        const r = Math.floor(Math.random() * 1000)
        const camp = new Campground({
            title: `${sample(descriptors)} ${sample(places)}`,
            description:
                'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam dolores vero perferendis laudantium, consequuntur voluptatibus nulla architecto, sit soluta esse iure sed labore ipsam a cum nihil atque molestiae deserunt!',
            location: `${cities[r].city}, ${cities[r].state}`,
            geometry: {
                type: "Point",
                coordinates: [
                    cities[r].longitude,
                    cities[r].latitude,
                ]
            },
            price: Math.floor(Math.random() * 50) + 15,
            images: images,
            author: '6499d857bdee98eb28637f93'
        })
        await camp.save()
    }
}

seedDB().then(() => {
    console.log('Database has been successfully seeded')
    mongoose.connection.close()
})
