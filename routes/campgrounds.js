const { storage } = require('../cloudinary')
const { isLoggedIn, validateCampground, isAuthor } = require('../middleware')
const express = require('express')
const router = express.Router()
const wrapAsync = require('../utils/wrapAsync')
const campgrounds = require('../controllers/campgrounds')
const multer = require('multer')
const upload = multer({ storage })

router.route('/')
    .get(wrapAsync(campgrounds.index))
    .post(isLoggedIn, upload.array('image'), validateCampground, wrapAsync(campgrounds.create))

router.get('/new', isLoggedIn, campgrounds.renderNewForm)

router.route('/:id')
    .get(wrapAsync(campgrounds.show))
    .put(isLoggedIn, isAuthor, upload.array('image'), validateCampground, wrapAsync(campgrounds.update))
    .delete(isLoggedIn, isAuthor, wrapAsync(campgrounds.delete))

router.get('/:id/edit', isLoggedIn, isAuthor, wrapAsync(campgrounds.renderEditForm))

module.exports = router